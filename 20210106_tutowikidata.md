## Créer une frise chronologique des directeurs de service d’archives

### A - Ajouter les données

#### 1 - Avoir la liste des directeurs

Dans un premier temps il faut rassembler les informations concernant les directeurs qui se sont succédés.

Il faut leur nom, prénom et date d’exercice. 

Ces informations peuvent se trouver :
* Sur le site du service,
* Sur la fiche Wikipédia sur le service,
* En recherchant dans la revue [Bibliothèque de l'école des chartes](https://www.persee.fr/collection/bec) sur Persée

#### 2 - Contrôler leur existence sur Wikidata

Pour chacun des noms trouver il faut les chercher dans Wikidata pour voir s'ils existent déjà.

Si le personne n'existe pas, il faut alors créer une entité nouvelle entité dans wikidata avec le maximum d'information sur la personne. Puis compléter comme ci-dessous

Si la personne existe il faut alors compléter les informations suivantes :
* Ajouter une déclaration : fonction (P39),
* Dans cette déclaration ajouter une valeur "directeur" (Q1162163), elle-même enrichie des qualificatifs suivants :
    * "de" (P642) + nom du service
    * date de début" (P580) + date de prise de fonction
    * "date de fin" (P582) + date de fin de fonction
    * "remplace" (P1366) + nom du directeur précédent
    * "remplacé par" (P1365) + nom du directeur suivant
   

[Louis Blancard](https://www.wikidata.org/wiki/Q18578072) peut être pris comme exemple à reproduire
  
### B - Utiliser les données
#### 1 - Contrôler le résultat dans Query

L'outil [Wikidata Query Service](https://query.wikidata.org/) permet de faire des recherches dans la base de données wikidata. Ceci se fait à partir de recherche en language SPARQL.

Vous trouverez [ici](https://query.wikidata.org/#%23Chronologie%20des%20directeurs%20d'un%20service%20d'archives%20%0A%0Aselect%20%3Fdirecteur%20%3FdirecteurLabel%20%3Fstart%20%3Fend%20%3FgenreLabel%20%3Fimg%20where%20%7B%0A%0A%3Fdirecteur%20p%3AP39%20%3Fposition.%20%0A%3Fposition%20ps%3AP39%20wd%3AQ1162163%3B%20%23recherche%20les%20directeurs%0A%0A%20%20%20%20%20%20%20%20pq%3AP642%20wd%3AQ19406829%20%3B%20%23Limite%20la%20recherche%20%C3%A0%20un%20service%20d'archives%20en%20indiquant%20le%20Q%20du%20service%0A%0A%20%20%20%20%20%20%20%20%20%20OPTIONAL%20%7B%20%3Fposition%20pq%3AP580%20%3Fstart.%20%7D%20%23recherche%20la%20date%20de%20d%C3%A9but%20d'exercice%0A%20%20%20%20%20%20%20%20%20%20OPTIONAL%20%7B%20%3Fposition%20pq%3AP582%20%3Fend.%20%7D%20%23recherche%20la%20date%20de%20fin%20d'exercice%0A%20%20%20%20%20%20%20%20%20%20OPTIONAL%20%7B%20%3Fdirecteur%20wdt%3AP21%20%3Fgenre.%20%7D%20%23recherche%20le%20genre%20de%20la%20personne%0A%20%20%20%20%20%20%20%20%20%20OPTIONAL%20%7B%20%3Fdirecteur%20wdt%3AP18%20%3Fimg.%20%7D%20%23recherche%20le%20genre%20de%20la%20personne%0A%0ASERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cfr%2Cen%22.%20%7D%0A%0A%7D%0A%0AORDER%20BY%20%3Fstart) la requête pour rechercher tous les directeurs d'un service. En plus, cette requête permet d'avoir les dates de début, de fin d'exercice, le genre de la personne et les images liés si les informations sont renseignées.

Si le résultat est conforme on peut alors créer un frise à partir de l'outil [Histropedia](http://histropedia.com/)

#### 2 - Créer sa frise chronologique

Histropedia propose l'outil [Wikidata Query Timeline](http://histropedia.com/showcase/wikidata-viewer.html) (encore en version bêta) de création de frise à partir de la requête que nous venons de faire sur le Wikidata Query Service.

Il suffit de copier la requête, comme par exemple ici pour les Archives des Bouches-du-Rhône :


#Chronologie des directeurs d'un service d'archives 

```
select ?directeur ?directeurLabel ?start ?end ?genreLabel ?img where {

?directeur p:P39 ?position. 
?position ps:P39 wd:Q1162163; #recherche les directeurs

        pq:P642 wd:Q2860505 ; #Limite la recherche à un service d'archives en indiquant le Q du service

          OPTIONAL { ?position pq:P580 ?start. } #recherche la date de début d'exercice
          OPTIONAL { ?position pq:P582 ?end. } #recherche la date de fin d'exercice
          OPTIONAL { ?directeur wdt:P21 ?genre. } #recherche le genre de la personne
          OPTIONAL { ?directeur wdt:P18 ?img. } #recherche le genre de la personne

SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],fr,en". }

}

ORDER BY ?start
```



Avant de générer la frise, il faut renseigner :
* Title = directeurLabel
* Start date = start
* End date = end
* Image = img
* Colour code by = genreLabel

Pour finir cliquer sur _Generate timeline_ 

La frise peut alors être partager via un lien que l'on peut copier en cliquant sur la flêche en haut à gauche de la page.

### C - Conclusion

La méthode proposée ici est totalement manuelle notamment pour la création ou l'enrichissement dans Wikidata. Des utilisateurs plus avncés de Wikidat pourrons utiliser des imports via CSV, ceci permettant de gagner du temps.

La requête proposer doit servir de base et peut être facilement enrichi selon ce que l'on veut voir dans sa frise.

Quelques exemples de propriétés pouvant être pertinentes :
* Lieu de naissance ou de mort,
* Distinctions reçues,
* Scolarité,
* Diplôme universitaire,
* Occupation (métier)…

L'enrichissement de wikidata en reliant les directeurs entre eux ouvre aussi la voie d'une exploitation par graph relationnel en utilisant des outils du type [QueryGraph](http://dataexplorer.hd.free.fr/QueryGraph/index.html?lang=fr)

**Cette exploitation de wikidata doit beaucoup à l'aide et aux conseils de Nicolas Vigneron (@belett). Un grand merci à lui.**
